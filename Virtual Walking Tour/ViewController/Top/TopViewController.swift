//
//  TopViewController.swift
//  Virtual Walking Tour
//
//  Created by keiji-sekiguchi on 2020/07/30.
//  Copyright © 2020 keiji-sekiguchi. All rights reserved.
//

import UIKit

class TopViewController: UIViewController {
    @IBAction func pushButtonA(button: UIButton) {
        pushWalkingAVC()
    }
    
    @IBAction func pushButtonO(button: UIButton) {
        pushWalkingOVC()
    }
    
    @IBAction func pushButtonS(button: UIButton) {
        pushWalkingSVC()
    }
    
    private func pushWalkingAVC() {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = sb.instantiateViewController(identifier: "WalkingViewController")
            if let walkingViewController = vc as? WalkingViewController {
                walkingViewController.routeId = .Alps
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func pushWalkingOVC() {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = sb.instantiateViewController(identifier: "WalkingViewController")
            if let walkingViewController = vc as? WalkingViewController {
                walkingViewController.routeId = .OmotesandoToNtj
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func pushWalkingSVC() {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = sb.instantiateViewController(identifier: "WalkingViewController")
            if let walkingViewController = vc as? WalkingViewController {
                walkingViewController.routeId = .Shirakawago
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
