//
//  WalkingViewController.swift
//  Virtual Walking Tour
//
//  Created by keiji-sekiguchi on 2020/07/30.
//  Copyright © 2020 keiji-sekiguchi. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreMotion

class WalkingViewController: UIViewController {
    
//    private let stride = 0.64
    private let stride = 10.0

    private var myPedometer: CMPedometer!
    private var panoView: GMSPanoramaView!
    private var totalDistance: Double = 0.0
    private var steps: Int = 0
    
    private var route : Route = getRoute(id: .PigeonAtMitaka)
    var routeId: RouteId = .PigeonAtMitaka {
        didSet {
            route = getRoute(id: routeId)
        }
    }
    
    
//    private var motionManager : CMMotionManager = CMMotionManager()

//    // テスト
//    private let walkPosition = [
//        CLLocationCoordinate2D(latitude: 35.66493, longitude: 139.713035),
//        CLLocationCoordinate2D(latitude: 35.66493, longitude: 139.713034),
//        CLLocationCoordinate2D(latitude: 35.664926, longitude: 139.713031),
//        CLLocationCoordinate2D(latitude: 35.664917, longitude: 139.713022),
//        CLLocationCoordinate2D(latitude: 35.665056, longitude: 139.712812),
//        CLLocationCoordinate2D(latitude: 35.665056, longitude: 139.712812),
//        CLLocationCoordinate2D(latitude: 35.665194, longitude: 139.712801),
//        CLLocationCoordinate2D(latitude: 35.665492, longitude: 139.713068),
//        CLLocationCoordinate2D(latitude: 35.665549, longitude: 139.713116),
//        CLLocationCoordinate2D(latitude: 35.666034, longitude: 139.713557),
//        CLLocationCoordinate2D(latitude: 35.666076, longitude: 139.713596),
//        CLLocationCoordinate2D(latitude: 35.666252, longitude: 139.713752),
//        CLLocationCoordinate2D(latitude: 35.666311, longitude: 139.713812),
//        CLLocationCoordinate2D(latitude: 35.666514, longitude: 139.713997),
//        CLLocationCoordinate2D(latitude: 35.666825, longitude: 139.714275),
//        CLLocationCoordinate2D(latitude: 35.666846, longitude: 139.714292),
//        CLLocationCoordinate2D(latitude: 35.66687, longitude: 139.714315),
//        CLLocationCoordinate2D(latitude: 35.667361, longitude: 139.714755)
//    ]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        panoView = GMSPanoramaView(frame: .zero)
        panoView.navigationGestures = false
        panoView.navigationLinksHidden = true
        panoView.delegate = self
        self.view = panoView
        
        totalDistance = route.getTotalDistance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let location = self.route.getLocation(meterFromStart: 0)
        panoView.moveNearCoordinate(location.0)
        panoView.camera = GMSPanoramaCamera(heading: location.1, pitch: 0, zoom: 1)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startWalk()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        myPedometer.stopUpdates()
    }
    
    func startWalk() {
        // 歩数計を生成.
        myPedometer = CMPedometer()

        // ペドメーター(歩数計)で計測開始.
        myPedometer.startUpdates(from: NSDate() as Date, withHandler: { (pedometerData, error) in
            if let e = error {
                print(e.localizedDescription)
                return
            }
            guard let data = pedometerData else {
                return
            }
                        
            self.steps = data.numberOfSteps.intValue
            let distance = Double(self.steps) * self.stride
            let location = self.route.getLocation(meterFromStart: distance)

            NSLog("coordinate:%f:%f, direction:%f", location.0.latitude, location.0.longitude, location.1)
            
            DispatchQueue.main.async {
                self.panoView.moveNearCoordinate(location.0)
                self.panoView.camera = GMSPanoramaCamera(heading: location.1, pitch: 0, zoom: 1)
            }
            
            if distance >= self.totalDistance {
                self.myPedometer.stopUpdates()
                self.pushFinishVC()
            }
        })
    }
    
    private func pushFinishVC() {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = sb.instantiateViewController(identifier: "FinishViewController")
            if let finishViewController = vc as? FinishViewController {
                finishViewController.steps = self.steps
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
//            self.performSegue(withIdentifier: "Finish", sender: nil)
        }
    }
}

extension WalkingViewController : GMSPanoramaViewDelegate {
    func panoramaView(_ panoramaView: GMSPanoramaView, didMove camera: GMSPanoramaCamera) {
        
    }
    
    func panoramaView(_ view: GMSPanoramaView, error: Error, onMoveNearCoordinate coordinate: CLLocationCoordinate2D) {
//        NSLog("error coordinate:%@", coordinate)
    }
    
}

extension CLLocationCoordinate2D {
    
    func walkPosition() -> CLLocationCoordinate2D {
        let stride = 0.1
        return CLLocationCoordinate2D(latitude: latitude + stride, longitude: longitude + stride)
    }
}
