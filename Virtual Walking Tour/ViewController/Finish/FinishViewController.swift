//
//  FinishViewController.swift
//  Virtual Walking Tour
//
//  Created by keiji-sekiguchi on 2020/07/30.
//  Copyright © 2020 keiji-sekiguchi. All rights reserved.
//

import UIKit
import CoreMotion

class FinishViewController: UIViewController {
    @IBOutlet var label: UILabel!
    
    @IBAction func didTapRoute(sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    var steps : Int = 0
    
    func addBackground(name: String) {
        // スクリーンサイズの取得
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height

        // スクリーンサイズにあわせてimageViewの配置
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        //imageViewに背景画像を表示
        imageViewBackground.image = UIImage(named: name)

        // 画像の表示モードを変更。
        imageViewBackground.contentMode = UIView.ContentMode.scaleAspectFill

        // subviewをメインビューに追加
        self.view.addSubview(imageViewBackground)
        // 加えたsubviewを、最背面に設置する
        self.view.sendSubviewToBack(imageViewBackground)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let kcal_ = 0.59 * Double(steps)
        let text_ = "消費カロリーは" + kcal_.description + "kcalです"
        label.text = text_ //表示させたい文字
        self.addBackground(name: "finish-buckground.jpg")

    }

    
}


