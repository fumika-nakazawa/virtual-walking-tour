//
//  Route.swift
//  Virtual Walking Tour
//
//  Created by kyota-fujikura on 2020/08/20.
//  Copyright © 2020 keiji-sekiguchi. All rights reserved.
//

import Foundation
import CoreLocation

private extension CLLocationCoordinate2D {
    func distance(to: CLLocationCoordinate2D) -> CLLocationDistance {
        let destination = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return CLLocation(latitude: latitude, longitude: longitude).distance(from: destination)
    }

    /*
     * selfから見たtoの向きを返す
     * see:  https://stackoverflow.com/questions/26998029/calculating-bearing-between-two-cllocation-points-in-swift
     */
    func direction(to : CLLocationCoordinate2D) -> CLLocationDirection {
        func degreesToRadians(_ degrees: Double) -> Double { return degrees * .pi / 180.0 }
        func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / .pi }

        let lat1 = degreesToRadians(self.latitude)
        let lon1 = degreesToRadians(self.longitude)

        let lat2 = degreesToRadians(to.latitude)
        let lon2 = degreesToRadians(to.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let dirRadians = atan2(y, x)

        return radiansToDegrees(dirRadians < 0 ? dirRadians + 2 * .pi : dirRadians)
    }

    /*
     * 内分点を返す
     * ex) ratio = 0.25
     *   (Self)--*-------(To)
     *           ↑ここを返す
     */
    func midPoint(to: CLLocationCoordinate2D, ratio: Double) -> CLLocationCoordinate2D {
        let ratio2 = 1.0 - ratio
        return CLLocationCoordinate2D(latitude: ratio2 * latitude + ratio * to.latitude, longitude: ratio2 * longitude + ratio * to.longitude)
    }
}

class Route {
    let coords : [CLLocationCoordinate2D]

    init(coords: [CLLocationCoordinate2D]) {
        self.coords = coords
    }

    func getStart() -> (CLLocationCoordinate2D, CLLocationDirection) {
        return (coords[0], coords[0].direction(to: coords[1]))
    }

    func getGoal() -> (CLLocationCoordinate2D, CLLocationDirection) {
        return (coords[coords.count - 1], coords[coords.count - 2].direction(to: coords[coords.count - 1]))
    }

    func getTotalDistance() -> Double {
        // NOTE: 何回も呼ぶなら事前計算しておいたほうがいいかも
        return zip(coords, coords.dropFirst()).lazy.map {
            $0.distance(to: $1)
        }.reduce(0.0, +)
    }

    func getLocation(meterFromStart: Double) -> (CLLocationCoordinate2D, CLLocationDirection) {
        // NOTE: 今の実装はO(N)なので、遅ければ事前に出発地からの距離計算しておいて二分探索する
        var currentMeter = 0.0
        for (i, coord) in coords.enumerated().dropFirst() {
            let prevCoord = coords[i - 1]
            let sectionDistance = prevCoord.distance(to: coord)

            if sectionDistance > 0 && meterFromStart < currentMeter + sectionDistance {
                let ratio = (meterFromStart - currentMeter) / Double(sectionDistance)
                let p = prevCoord.midPoint(to: coord, ratio: ratio)
                return (p, p.direction(to: coord))
            }

            currentMeter += sectionDistance
        }

        return getGoal()
    }
}
